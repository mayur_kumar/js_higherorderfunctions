function map(elements, cb) {
    const arr = []
    for(let i=0;i<elements.length;i++){
        let result = cb(elements[i])
        arr.push(result)
    }
    return arr
}

export{map}

