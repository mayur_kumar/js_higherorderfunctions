import { flatten } from "../flatten.js";

const nestedArray = [1, [2], [[3]], [[[4]]]]; 

let flattenArr = flatten(nestedArray)
 
console.log(flattenArr)