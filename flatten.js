

function flatten(elements) {
    let flattenArr=[]
    for(let i=0;i<elements.length;i++){
        if(Array.isArray(elements[i])){
            flattenArr = flattenArr.concat(flatten(elements[i]))
        }else{
            flattenArr.push(elements[i])
        }

    }
    return flattenArr
}


export{flatten}