function reduce(elements, cb, startingValue) {
    let accumulator
    if (!startingValue){
        accumulator = elements[0]
        for (let i=1 ;i<elements.length;i++){
            accumulator = cb(accumulator,elements[i])
        }
    }

    else{
        accumulator = startingValue
        for (let i=0 ;i<elements.length;i++){
            accumulator = cb(accumulator,elements[i])
        }
    }
    

    return accumulator 
}


export{reduce}

